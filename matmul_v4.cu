#include <iostream>
#include <cstdio>
#include <vector>
#include <ctime>
#include <cstring>
#include <cmath>
#include <omp.h>

#define VECTOR_BIT_WIDTH 1024
#define VECTOR_BYTE_WIDTH VECTOR_BIT_WIDTH / 8
#define NUM_BIT_SIZE 32
#define NUM_BYTE_SIZE NUM_BIT_SIZE / 8
#define VECTOR_NUM_WIDTH VECTOR_BIT_WIDTH / NUM_BIT_SIZE
#define NUM_THREADS_Y 32
#define ROWS_PER_THREAD 32

using namespace std;

void print_csr_matrix (
	uint32_t 	size,
	uint32_t*	row_index,
	uint32_t*	col_index)
{	
	cout << "size = " << size << endl;
	cout << "row_index: ";
	for (uint32_t i = 0; i < size + 1; i++) {
		cout << row_index[i] << " ";
	}
	cout << endl << "col_index: ";
	for (uint32_t i = 0; i < size; i++) {
		for (uint32_t j = 0; j < row_index[i + 1] - row_index[i]; j++){
			cout << col_index[i] << " ";
		}
	}
	cout << endl;
}

void print_vector (
	uint32_t*	vec,
	uint32_t	size)
{
	for (uint32_t row = 0; row < size; row++) {
		for (uint32_t uint_num = 0; uint_num < VECTOR_BIT_WIDTH / 32; uint_num++) {
			for (int bit_number = 31; bit_number >= 0; bit_number--) {
				uint32_t to_print = (vec[row * VECTOR_NUM_WIDTH + uint_num] & (1 << bit_number)) >> bit_number;
				cout << to_print;
			}
			cout << "  ";
		}
		cout << endl;
	}
}

void read_bin_matrix (
	char const*	path,
	uint32_t	size,
	uint32_t* 	row_index,
	uint32_t* 	col_index)
{
	uint32_t 	elem_amnt = 0;
	uint32_t 	elem_col;
	FILE *matrix_file = fopen(path, "rb");

	row_index[0] = 0;
	for (uint32_t i = 1; i < size + 1; i++)
	{
		fread(&elem_amnt, sizeof(uint32_t), 1, matrix_file);
		row_index[i] = row_index[i - 1] + elem_amnt;
    	for (uint32_t j = 0; j < elem_amnt; j++) {
    		fread(&elem_col, sizeof(uint32_t), 1, matrix_file);
    		col_index[row_index[i - 1] + j] = elem_col;
    	}
	}
	fclose(matrix_file);
}

void count_matrix (
	char const*	path,
	uint32_t*	size,
	uint32_t*	nonzero)
{	
	FILE *matrix_file = fopen(path, "rb");
	uint32_t elem_amnt;
	*nonzero = 0;
	*size = 0;
	while (fread(&elem_amnt, sizeof(uint32_t), 1, matrix_file) == 1)
	{
		(*size)++;
		*nonzero += elem_amnt;
    	fseek(matrix_file, elem_amnt * sizeof(uint32_t), SEEK_CUR);
	}
	fclose(matrix_file);
}

void generate_vec (
	uint32_t* 	vec,
	uint32_t 	size)
{
	srand (time(NULL));
	for (int i = 0; i < size * VECTOR_NUM_WIDTH; i++) {
		// vec[i] = rand();
		vec[i] = 0xFFFFFFFF;
	}
}

void add_zeros (
	uint32_t*	row_index,
	uint32_t	size,
	uint32_t	enhanced_size)
{
	for (uint32_t i = size + 1; i < enhanced_size + 1; i++) {
		row_index[i] = row_index[size];
	}
}

__global__
void gpu_matmul_v4 (
	uint32_t*	row_index,
	uint32_t*	col_index,
	uint32_t*	vec,
	uint32_t*	res
	)
{
	//uint32_t row = NUM_THREADS_Y * blockIdx.x + threadIdx.y;
	uint32_t uint_num = threadIdx.x;
	
	for (uint32_t row = ROWS_PER_THREAD * NUM_THREADS_Y * blockIdx.x;
	row < ROWS_PER_THREAD * NUM_THREADS_Y * (blockIdx.x + 1); row++) {

		uint32_t sum = 0;
		for (uint32_t i = row_index[row]; i < row_index[row + 1]; i++) {
			sum ^= vec[col_index[i] * VECTOR_NUM_WIDTH + uint_num];
		}
		res[row * VECTOR_NUM_WIDTH + uint_num] = sum;

	}
}

void call_gpu_matmul_v4 (
	uint32_t*	row_index,
	uint32_t*	col_index,
	uint32_t	size,
	uint32_t*	vec,
	uint32_t*	res
	)
{	
	gpu_matmul_v4<<<size / NUM_THREADS_Y / ROWS_PER_THREAD,
		dim3(VECTOR_NUM_WIDTH, 2, 1)>>>(row_index, col_index, vec, res);

	cudaDeviceSynchronize();
}

int main(int argc, char const *argv[])
{
	uint32_t*	row_index;
	uint32_t*	col_index;
	uint32_t*	vec;
	uint32_t*	res;
	uint32_t 	size;
	uint32_t	enhanced_size;
	uint32_t	nonzero;
	double 		c_start;
	double 		c_end;
	bool		need_print = (argc > 2 && strncmp(argv[2], "-p", 2) == 0);

	cout << argv[1] << endl;
	cout << "NUM_THREADS_Y = " << NUM_THREADS_Y << endl;
	cout << "ROWS_PER_THREAD = " << ROWS_PER_THREAD << endl;

	count_matrix(argv[1], &size, &nonzero);
	cout << "size = " << size << " nonzero = " << nonzero << endl;

	enhanced_size = size + size % (NUM_THREADS_Y * ROWS_PER_THREAD);
	cout << "enhanced_size = " << enhanced_size << endl;

	cudaMallocManaged(&row_index, (enhanced_size + 1) * sizeof(uint32_t));
	cudaMallocManaged(&col_index, nonzero * sizeof(uint32_t));
	cudaMallocManaged(&vec, size * VECTOR_BYTE_WIDTH);
	cudaMallocManaged(&res, enhanced_size * VECTOR_BYTE_WIDTH);


	cout << endl;
	cout << "read bin ";
	c_start = omp_get_wtime();
	read_bin_matrix(argv[1], size, row_index, col_index);
	c_end = omp_get_wtime();
	double read_time = (c_end - c_start);
	cout << "in " << read_time << " sec" << endl;

	add_zeros(row_index, size, enhanced_size);

	if (need_print) {
		cout << endl;
		cout << "CSR Matrix (zeroes added)" << endl;
		print_csr_matrix(enhanced_size, row_index, col_index);
	}

	cout << endl;
	cout << "generate vector" << endl;
	c_start = omp_get_wtime();
	generate_vec(vec, size);
	c_end = omp_get_wtime();
	double vec_gen_time = (c_end - c_start);
	cout << " in " << vec_gen_time << " sec" << endl;

	if (need_print) {
		cout << "vector:" << endl;
		print_vector(vec, size);
	}
	
	cout << endl;
	cout << "gpu_matmul_v4 ";
	c_start = omp_get_wtime();
	call_gpu_matmul_v4(row_index, col_index, enhanced_size, vec, res);
	c_end = omp_get_wtime();
	double mult_time = (c_end - c_start);
	cout << "finished in " << mult_time << " sec" << endl;

	cout << "gpu_matmul_v4 after load ";
	c_start = omp_get_wtime();
	call_gpu_matmul_v4(row_index, col_index, enhanced_size, vec, res);
	c_end = omp_get_wtime();
	double mult_time2 = (c_end - c_start);
	cout << "finished in " << mult_time2 << " sec" << endl;

	if (need_print) {
		cout << "result:" << endl;
		print_vector(res, size);
	}

	double perfect_time = (long double) nonzero * 128 / 114000000000;
	cout << "approximate \"perfect\" time considering GPU speed " << 114 << "GB/sec is ";
	cout << perfect_time << endl;


	double perfect_time_per_bit =  perfect_time / VECTOR_BIT_WIDTH;
	double real_time_per_bit = mult_time2 / VECTOR_BIT_WIDTH;
	cout << endl;
	cout << "time per vector column: " << real_time_per_bit << " sec" << endl;
	cout << "perfect time per vector column: " << perfect_time_per_bit << " sec" << endl;

	cout << endl;
	cout << "real / perfect = " << real_time_per_bit / perfect_time_per_bit << endl;
	
	
	return 0;
}