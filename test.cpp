#include <iostream>
#include <cstdio>
#include <stdint.h>

using namespace std;

int main(int argc, char const *argv[])
{
	FILE *matrix_file = fopen("imatr.bin", "wb");
	unsigned buffer[] = {1, 0, 1, 1, 1, 2, 1, 3, 1, 4};
	fwrite(buffer, sizeof(unsigned), sizeof(buffer) / sizeof(unsigned), matrix_file);
	return 0;
}